<?php
namespace modelos;


class Perfil extends Conexion{
    
    public function __construct(){
        parent::__construct();
    }
    static function perfilGeneral(){ 
        ?>
        <div class="opcionesPerfil">
        <a href="index.php?controller=General&action=perfil&opcion=publicar">Publicar</a>   
        <a href="index.php?controller=General&action=perfil&opcion=configurar">Información de perfil</a>  
        <a id="salida" href="index.php?controller=Principal&action=salir">salir</a>  
        </div>
        <?php
    }
    static function publicaciones(){        
        ?>        
        <div class="insertados">
        <h1>Publicar</h1>
            <form action="index.php?controller=General&action=publicacion" method="POST" enctype="multipart/form-data">
                Texto<br>
                <textarea name="texto" id="" cols="30" rows="10" placeholder="Agregar descripcion" required></textarea><br><br>            
                <br>Imagen Principal (opcional)<br>
                <input type="file" name="foto">            
                <br>clasificación<br>
                <select name="tipoc">
                    <option value="critica">Critica</option>
                    <option value="receta">Receta de cocina</option>
                    <option value="opinion">Opinión</option>                
                    <option value="tip">Tip</option>
                </select><br><br>
                <input type="submit" value="Publicar"><br>
            </form>
        </div>
        
        <?php
    }
    static function publicacion(){
        $conexion=new Conexion();
        if(isset($_REQUEST['texto'])&& !empty($_REQUEST['texto'])){
            $id_u=$_SESSION['id_usuario'];
            $texto=$_REQUEST['texto'];        
            $clasificacion=$_REQUEST['tipoc'];
            
            $comprobar=$conexion->conect->query("SELECT max(id_publicacion) as id_Pro FROM publicaciones");
            $resultado=$comprobar->fetch_assoc();  
            $id_publicar=$resultado['id_Pro'] + 1;
            
            $insertar="INSERT INTO publicaciones(id_publicacion,clasificacion,texto,id_usuario) values($id_publicar,'$clasificacion','$texto',$id_u)";    
            $conexion->conect->query($insertar);
            
            
            if(isset($_REQUEST['foto']) || !empty($_FILES['foto'])){    
                $tamArchivo = $_FILES['foto']['size'];
                $imgSubida = fopen($_FILES['foto']['tmp_name'], 'r');
                $binImg = fread($imgSubida, $tamArchivo);
                $binImg = mysqli_escape_string($conexion->conect, $binImg);                
                $insertarI="UPDATE publicaciones set imagen='$binImg' where id_publicacion=$id_publicar";
                $conexion->conect->query($insertarI);
            
            }
            
            
        }
        header('Location:index.php?controller=Principal&action=ingreso');
    }
    static function editar(){
        $conexion=new Conexion();
        $id_u=$_SESSION['id_usuario'];
        $queryProductos=$conexion->conect->query("SELECT id_Publicacion,clasificacion,texto,imagen,id_usuario FROM publicaciones Where id_Publicacion=".$_GET['publicacion']); 
        $resultados=$queryProductos->fetch_assoc();  
        ?>        
        <div class="insertados">
        <h1>Publicar</h1>
            <form action="index.php?controller=General&action=actualizacion&publicacion=<?php echo $_GET['publicacion']; ?>" method="POST" enctype="multipart/form-data">
                Texto<br>
                <textarea name="texto" id="" cols="30" rows="10" placeholder="Agregar descripcion" required><?php echo $resultados['texto']; ?></textarea><br><br>            
                <br>Imagen Principal (opcional)<br>
                <input type="file" name="foto">    
                <?php  echo ' <img alt="Imagen no disponible" width=60px height=60px id="producto" src="data:image/jpg;base64,'.base64_encode($resultados['imagen']).'" >'; ?>        
                <br>clasificación<br>
                <select name="tipoc">
                    <option value="critica">Critica</option>
                    <option value="receta">Receta de cocina</option>
                    <option value="opinion">Opinión</option>                
                    <option value="tip">Tip</option>
                </select><br><br>
                <input type="submit" value="Editar"><br>
            </form>
        </div>
        
        <?php
    }
    static function eliminar(){
        $conexion=new Conexion();
        $id_u=$_SESSION['id_usuario'];
        $eliminar="DELETE FROM publicaciones WHERE id_publicacion=".$_GET['publicacion'];
        $conexion->conect->query($eliminar);			
        echo "Publicación Eliminada";
        echo "<a href='index.php?controller=Principal&action=ingreso'>Volver</a>";                             
        $insertar="INSERT INTO actividad values($id_u,'Eliminaste de publicacion',CURRENT_DATE(),CURRENT_TIME())";    
        $conexion->conect->query($insertar);
    }
    static function configurarPerfil(){
        ?>
        <div class="insertados">
                <h1>Información general</h1>
                <?php
                $conexion=new Conexion();
                $id_usuario=$_SESSION['id_usuario'];
                
                $queryProducto=$conexion->conect->query("SELECT * FROM usuarios WHERE id_usuario=".$id_usuario);
                $resultados=$queryProducto->fetch_assoc();
                ?>
                <form action="index.php?controller=General&action=actualizar" method="POST" enctype="multipart/form-data">
                    <br>
                    <?php 
                    
                    echo "<br>";
                    $_SESSION['id_usuario']=$resultados['id_usuario'];
                    ?>
                    Nombre <br>
                    <input type="text" name="nombre" placeholder="Introduce nombre" value="<?php echo $resultados['nombre']; ?>" required><br>
                    <br>correo<br>
                    <label for=""><?php echo $resultados['correo']; ?></label>
                    <br>
                    <br>
                    Descripcion<br>
                    <textarea name="descripcion" id="" cols="30" rows="10" placeholder="Agregar descripcion"><?php echo $resultados['descripcion']; ?></textarea><br><br>
                    Zona<br>
                    <textarea name="zona" id="" cols="30" rows="10" placeholder="Agregar zona"><?php echo $resultados['zona']; ?></textarea><br><br>
                    
                    <br>Imagen Principal<br>
                    <input type="file" name="foto">
                    <?php  echo ' <img alt="Imagen no disponible" width=60px height=60px id="producto" src="data:image/jpg;base64,'.base64_encode($resultados['foto_perfil']).'" >'; ?>

                    <br><br>
                    <input type="submit" value="actualizar"><br>
                </form>
        </div>
        <?php
    }
    static function actualizarPublicacion(){
        $conexion=new Conexion();
        if(isset($_REQUEST['texto'])&& !empty($_REQUEST['texto'])){
            $id_u=$_SESSION['id_usuario'];
            $texto=$_REQUEST['texto'];        
            $clasificacion=$_REQUEST['tipoc'];            
            $insertar="UPDATE publicaciones set clasificacion='$clasificacion',texto='$texto' where id_publicacion=".$_GET['publicacion'];
            $conexion->conect->query($insertar);
            
            
            if(isset($_FILES['foto']) || !empty($_FILES['foto'])){     
                $tamArchivo = $_FILES['foto']['size'];
                $imgSubida = fopen($_FILES['foto']['tmp_name'], 'r');
                $binImg = fread($imgSubida, $tamArchivo);
                if($binImg!=NULL){
                    $binImg = mysqli_escape_string($conexion->conect, $binImg);                
                    $insertarI="UPDATE publicaciones set imagen='$binImg' where id_publicacion=".$_GET['publicacion'];
                    $conexion->conect->query($insertarI);
                }
                
            
            }
            
            
        }
        header('Location:index.php?controller=Principal&action=ingreso');
        $insertar="INSERT INTO actividad values($id_u,'Edicion de publicacion',CURRENT_DATE(),CURRENT_TIME())";    
            $conexion->conect->query($insertar);
    }
    static function datosActualizar(){
        $conexion=new Conexion();
        if(isset($_REQUEST['nombre'])&& !empty($_REQUEST['nombre'])){
            $id_u=$_SESSION['id_usuario'];
            $nombre=$_REQUEST['nombre'];        
            $zona=$_REQUEST['zona'];
            $descripcion=$_REQUEST['descripcion'];
            
            $actualizando="UPDATE usuarios SET nombre='$nombre',descripcion='$descripcion', zona='$zona' WHERE id_usuario = $id_u";    
            $conexion->conect->query($actualizando);
            print_r($actualizando);
            if(isset($_REQUEST['foto']) || !empty($_FILES['foto'])){    
                $tamArchivo = $_FILES['foto']['size'];
                $imgSubida = fopen($_FILES['foto']['tmp_name'], 'r');
                $binImg = fread($imgSubida, $tamArchivo);
                $binImg = mysqli_escape_string($conexion->conect, $binImg);                
                $insertarI="UPDATE usuarios set foto_perfil='$binImg' where id_usuario=$id_u";
                $conexion->conect->query($insertarI);
            }
            
            
        }
        header('Location:index.php?controller=Principal&action=ingreso');  
    }

}

?>
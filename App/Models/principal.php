<?php
namespace modelos;
class Principal extends Conexion{
public $id_usuario;
public $nombre;
public $correo;
public $contrasenia;
public $llave;
public $zona;
public $descripcion;
public function __construct(){
  parent::__construct();  
}
static function comprobardo($usuario,$contrasenia){
  $conexion=new Conexion();
  $preparar=mysqli_prepare($conexion->conect,"SELECT * FROM usuarios WHERE nombre=? AND 
  AES_DECRYPT(contrasenia,'llave')=?");
  $preparar->bind_param("ss",$usuario,$contrasenia);
  $preparar->execute();
  $siesta=  $preparar->get_result();
  return $siesta->fetch_assoc(); 
}
static function imprimir(){
  echo "<div class='catalogo'>";
  $conexion=new Conexion();
  $id_u=$_SESSION['id_usuario'];
  $queryProductos=$conexion->conect->query("SELECT id_Publicacion,clasificacion,texto,imagen,id_usuario FROM publicaciones order by id_Publicacion desc"); 
    $resultados=$queryProductos->fetch_assoc();  
    do{
    
    if($resultados['id_Publicacion']!=0){   
    
        echo "<div class='tarjetas'>";
        if($resultados['imagen']!=null){
          echo '<img alt="Imagen no disponible" src="data:image/jpg;base64, '.base64_encode($resultados['imagen']).'">';
        }        
        echo "<h4>".$resultados['clasificacion']."</h4>"; 
        echo "<h5> $ ".$resultados['id_usuario']."</h5>";
        echo "<h5>".$resultados['texto']."...</h5>";  
        if($resultados['id_usuario']==$id_u){               
          $id_publica=$resultados['id_Publicacion'];
          echo "<a href='index.php?controller=General&action=editar&publicacion=$id_publica'> Editar</a> <br><br>";
          echo "<a href='index.php?controller=General&action=eliminar&publicacion=$id_publica'> Eliminar</a> <br><br>";
        }
        echo "</div>";
    }else{
      echo"<h1 align=center>ERROR 404 |</h1> <br>";
      echo"<h1 align=center>| Elementos no encontrados o inexistentes</h1>";
    }
           
    }while($resultados=$queryProductos->fetch_assoc());
    echo "</div>";
        
}
static function menu(){
  ?>
  <header class="inicio">
        <nav class="menu1">
            <!-- Menu y buscador-->

            
                <ul class="menu">
                  <?php 
                    $conexion=new Conexion();
                    $id_usuario=$_SESSION['id_usuario'];                
                    $queryProducto=$conexion->conect->query("SELECT foto_perfil,nombre FROM usuarios WHERE id_usuario=".$id_usuario);
                    $resultados=$queryProducto->fetch_assoc();
                    echo '<li>'.$resultados['nombre'].'<img alt="Imagen no disponible" width=90px height=90px src="data:image/jpg;base64, '.base64_encode($resultados['foto_perfil']).'"</li>';
                  ?>
                    <li><a href="index.php?controller=General&action=todo" class="btn1" name="todo">Publicaciones</a></li>
                    <li><a href="index.php?controller=General&action=actividad" class="btn1" name="actividad">Actividad</a></li>
                    <li><a href="index.php?controller=General&action=perfil&opcion=publicar" class="btn1" name="perfil">Perfil</a></li>
                </ul>
            
            <!--Logotipo-->

        </nav>
    </header>
    <?php
}
static function actividades(){
  $conexion=new Conexion();
  $id_u=$_SESSION['id_usuario'];
  $queryProductos=$conexion->conect->query("SELECT* FROM actividad where id_usuario=$id_u order by fecha ASC "); 
  $resultados=$queryProductos->fetch_assoc(); 
  if($resultados!=NULL){
    do{
    echo "<br> <label class='actividadesLabel'> Realizaste un/una: ".$resultados['actividad']."</label>";
    echo "<br> <label class='actividadesLabel'> EN la fecha: ".$resultados['fecha']."</label>";
    echo "<br> <label class='actividadesLabel'> A la hora: ".$resultados['hora']."</label><br>";
    }while($resultados=$queryProductos->fetch_assoc());
  } 
}
static function registro($usuario,$correo,$contrasenia,$zona){
  $conexion=new Conexion();
  $preparar=mysqli_prepare($conexion->conect,
  "INSERT INTO usuarios(id_usuario,nombre,correo,contrasenia,zona) 
  VALUES(NULL,?,?,AES_ENCRYPT(?,'llave'),?)");
  $preparar->bind_param("ssss",$usuario,$correo,$contrasenia,$zona);
  $preparar->execute();
  $insertado=  $preparar->get_result();
  print_r($insertado);
  return $insertado;
}
}
?>
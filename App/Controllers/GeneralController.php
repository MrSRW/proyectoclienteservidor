<?php
require 'App/Models/conexion.php';
require 'App/Models/principal.php';
require 'App/Models/perfil.php';
use conectar\Conexion;
use modelos\Principal;
use modelos\Perfil;

class GeneralController{
    public function __construct(){
        if($_GET["action"]=="todo"||$_GET["action"]=="actividad"||$_GET["action"]=="perfil"){
            
            if(!isset($_SESSION['usuario'])){
                echo "no has iniciado sesion";
                header('Location:index.php?controller=Principal&action=iniciar');
            }
        }
    }
    public function todo(){    
        require 'app/view/general.php';
        $menu=principal::menu();     
        echo "Estás en todo";
        $publicaciones=principal::imprimir(); 
    }
    public function actividad(){    
        require 'app/view/general.php';
        $menu=principal::menu();         
        echo "Estás en actividad";
        $actividades=principal::actividades();
        
    }
    public function perfil(){
        require 'app/view/general.php';
        $menu=principal::menu(); 
        $perfil=perfil::perfilGeneral(); 
        echo "Estás en perfil";
        $opcion=$_GET['opcion'];
        $this->{$opcion}();
}
    public function publicar(){
        $configuracion=perfil::publicaciones();
    }
    public function publicacion(){
        $configuracion=perfil::publicacion();
    }
    public function actualizacion(){
        $configuracion=perfil::actualizarPublicacion();
    }
    public function configurar(){
        $configuracion=perfil::configurarPerfil();
    }
    public function actualizar(){
        $configuracion=perfil::datosActualizar();
    }
    public function editar(){
        require 'app/view/general.php';
        $menu=principal::menu(); 
        perfil::editar();
    }
    public function eliminar(){
        require 'app/view/general.php';
        perfil::eliminar();
    }
}

?>